$('.itemsLink').on('click', function(e){
	e.preventDefault();
	var $this = $(this);
	var $url = $this.prop('href');

	$.ajax({
		url: $url,
		success: function(data){
			$this.addClass('active');
			$this.parent().siblings().children().removeClass('active');
			deleteHtml($this);
			loadItems($this,data);
		}
	});
});

function deleteHtml($this){
	$this.parents('.previews-row__title').next('.contentSlider').children().remove();
};
function loadItems($this,data){
	$this.parents('.previews-row__title').next('.contentSlider').append(data);
	var slider = $this.parents('.previews-row__title').next('.contentSlider').children();
	if($(slider[0]).hasClass('itemsSliderFour')){
		itemsSliderFourInit(slider[0]);
	} else {
		itemsSliderSixInit(slider[0]);
	}
	tooltipsInit();
	infoClick();
};