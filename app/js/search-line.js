function searchLineInit(){
	var buttonWidth = $('#lineMoreBtn').outerWidth();
	var filterContainer = $('#mainList');
	var itemsMenuWidth = 0;

	filterContainer.css({'margin-right':buttonWidth+'px'});

	var filterContainerWidth = filterContainer.outerWidth();

	var removedItems = $('#moreDrop').children().remove();
	$('#mainList').append(removedItems);

	$('#mainList li').each(function(){
		itemsMenuWidth = itemsMenuWidth + $(this).outerWidth();
		if(itemsMenuWidth >= filterContainerWidth){
			var removedElement = $(this).remove();
			$('#moreDrop').append(removedElement);
			$('#lineMoreBtn').css({'display':'inline-block'});			
			$('.search-line-btn').css({'opacity':'1'});			
		} else {
			$('#lineMoreBtn').css({'display':'none'});			
		}
	});
};

function initMoreDropDownMenu() {
	$('#lineMoreBtn').on('click',function(){
		var $this = $(this);
		if($this.hasClass('active')){
			$('.searchLineDrop').slideUp(200);
			$this.removeClass('active');
		} else {
			$this.addClass('active');
			$('.searchLineDrop').slideDown(200);
		}
	});
}