function openMobileCatalog(){
	$('.mobileCatBtn').children().on('click',function(){
		var $this = $(this).parent();
		if(!$this.hasClass('active')){
			$this.addClass('active').parent().next().slideDown('fast');
		} else {
			$this.removeClass('active').parent().next().slideUp('fast');
		}
	});
};