$(document).ready(function() {

    // cabinet-phystabs-list
    $('.cabinet-phystabs-list').slick({
        slidesToShow: 6,
        responsive: [{
            breakpoint: 1200,
            settings: {
                slidesToShow: 4,
            }
        }],
    });
    // END:cabinet-phystabs-list

    // jquery-ui style
    $(".new-ui-selectmenu").selectmenu();
    // END:jquery-ui style

    // maskPhone
    $('.mask--phone').mask('+7 000 000 000');
    // END:maskPhone

    // formRegion
    $('.cabinet-phys-element--region').on('click', '.form-action-btn', function(event) {
        event.preventDefault();

        var optionVal = $(this).parents('.cabinet-phys-element').find('select option:selected').val();
        var closeBtn = '<button type="button" class="close removeElement"><span aria-hidden="true"><i class="fa fa-times" aria-hidden="true"></i></span></button>';

        var addFormElementsArgs = {
            template: '<li>' + optionVal + closeBtn + '</li>',
            appendPlace: $(this).parents('.cabinet-phys-element').find('.cabinet-phys-element__dynamic-list'),
        };
        addFormElements(addFormElementsArgs);

        var addFormElementsPopupArgs = {
            template: '<li class="phys-popup-regions__item">' + optionVal + closeBtn + '</li>',
            appendPlace: $('.phys-popup-regions__list'),
        };
        addFormElements(addFormElementsPopupArgs);
    });

    $('body').on('click', '.removeElement', function(event) {
        event.preventDefault();

        var removeFormElementsArgs = {
            elementToRemove: $(this).parents('li'),
        };
        removeFormElements(removeFormElementsArgs);
    });
    // END:formRegion

    // add new communication fields (multiple/single)
    $('.cabinet-phys-element--communication').on('click', '.form-action-btn--add', function(event) {
        event.preventDefault();
        var cabinetPhysElement = $(this).parents('.cabinet-phys-element');

            var formElementHtml = cabinetPhysElement.find('.form-element').html();

            var addFormElementsArgs = {
                template: '<div class="form-element row">' + formElementHtml + '</div>',
                appendPlace: cabinetPhysElement.find('.form-elements'),
            };
            addFormElements(addFormElementsArgs);

            $('.mask--phone').mask('+7 000 000 000');
    });
    // END:add new communication fields (multiple/single)

    // achievements-wrap ADD
    $('.achievements-wrap').on('click', '.form-action-btn--add', function(event) {
        event.preventDefault();

        var formElementHtml = $(this).parents('.achievements-wrap').find('.achievements-wrap__item').html();

        var addFormElementsArgs = {
            template: '<div class="achievements-wrap__item achievements-wrap__item--new row clearfix">'+formElementHtml+'</div>',
            appendPlace: $(this).parents('.achievements-wrap').find('.achievements-wrap__items'),
        };
        addFormElements(addFormElementsArgs);
    });
    // END:achievements-wrap ADD

    // achievements-wrap REMOVE
    $('.achievements-wrap').on('click', '.close', function(event) {
        event.preventDefault();

        var removeFormElementsArgs = {
            elementToRemove: $(this).parents('.achievements-wrap__item'),
        };
        removeFormElements(removeFormElementsArgs);
    });
    // END:achievements-wrap REMOVE

    // photoblock-images
    $('.photoblock-images').on('click', '.photoblock-images__tooltip-action-btn', function(event) {
        event.preventDefault();
        
        var thisImageSrc = $(this).parents('.photoblock-images').find('img').attr('src');
        $('.cabinet-phys-avatar__imgblock').find('img').attr('src', thisImageSrc);
    });
    // END:photoblock-images

    // cabinet-catalog-list__return-btn
    $('.cabinet-catalog-list__return-btn').on('click', '.selector', function(event) {
        event.preventDefault();
        

    });
    // END:cabinet-catalog-list__return-btn

    // dynamicTabs
    $('.cabinet-phystabs-list li').on('click', function(event) {
        event.preventDefault();
        
        var tabOptions = {
            tabLink: $(this).find('a'),
            tabsSection: $('.cabinet-phys-form__section'),
        };

        dynamicTabs(tabOptions);
    });
    // END:dynamicTabs
});
