// addFormElements
function addFormElements(args) {
    if (args.template) {
        $(args.appendPlace).append(args.template);
    }
}
// END:addFormElements

// removeFormElements
function removeFormElements(args) {
    if (args.elementToRemove) {
        $(args.elementToRemove).remove();
    }
}
// END:removeFormElements

// dynamicTabs
function dynamicTabs(options) {
    $(options.tabsSection).each(function(index, el) {
        var tabLinkHref = $(options.tabLink).attr('href').slice(1);
        
        $(options.tabLink).parent().addClass('activeTab').siblings().removeClass('activeTab');
        
        if ($(el).attr('id') === tabLinkHref) {

            $(el).fadeIn().siblings().fadeOut();
        }
    });
}
// END:dynamicTabs
