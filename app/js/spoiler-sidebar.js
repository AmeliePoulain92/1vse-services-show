function spoilerSidebarInit(name){
	$(name).each(function(){
		var $this = $(this);
		if($(this).children().length>4){

			var itemsHeight = $(this).children().outerHeight();
			var itemsLength = $(this).children().length;
			var overalheight = itemsHeight * itemsLength + 'px';
			var button = $(this).next().children();
			
			button.on('click',function(){
				if(!$(this).hasClass('active')){

					$(this).addClass('active');
					$(this).parent().prev().animate({
						height: overalheight
					}, 200);

				}else{

					$(this).removeClass('active');
					$(this).parent().prev().animate({
						height:'140px'
					}, 200);

				}
			});

		} else {

			$(this).next().hide();

		}
	});
};