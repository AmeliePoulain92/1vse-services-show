var availableTags = [
  "ActionScript",
  "AppleScript",
  "Asp",
  "BASIC",
  "C",
  "C++",
  "Clojure",
  "COBOL",
  "ColdFusion",
  "Erlang",
  "Fortran",
  "Groovy",
  "Haskell",
  "Java",
  "JavaScript",
  "Lisp",
  "Perl",
  "PHP",
  "Python",
  "Ruby",
  "Scala",
  "Scheme"
];

// SEARCH_AUTOCOMPLITE
$("#search-comlite").autocomplete({
  source: availableTags,
  classes: {
    "ui-autocomplete": "search-autocomplite-drop"
  },
  open: function(event,ui){
    var leftPosition = $(event.target).offset().left;
    $('.search-autocomplite-drop .ui-menu-item-wrapper').css({'padding-left':leftPosition+15+'px'});
  }
});

// CITY_AUTOCOMPLITE
$("#city-comlite").autocomplete({
  source: availableTags,
  classes: {
    "ui-autocomplete": "cities-autocomplite__list"
  }
});