'use strict'
$(window).on("load", function(){
	filterRangeInit();
	infoClick();
	horizontalView(); // Items view init
	tabsSlider();
	spinnerCountInit();
	selectMenuInit();
	hoverBlackout('.sidebar-catalog');
	hoverBlackout('.dropdown-catalog','.main-menu');
	itemsSliderSixInit('.itemsSliderSix');
	itemsSliderFourInit('.itemsSliderFour');
	cartProductImagesInit();
	cartInfoTabsInit();
	searchLineInit();
	initMoreDropDownMenu();
	tooltipsInit();
	compareSLiderInit();
	initReviewStars();
	initVerticalSlider();
	openComplainPopup();
	showReviewForm();
	hoverMenuItems();
});

$(document).ready(function(){
	spoilerSidebarInit('.shownFilterList');
	openMobileCatalog();
});

$(window).on("resize", function(){
	searchLineInit();
});
$(window).on("scroll", function(event){
	fixMenu(event);
});

function filterRangeInit(){
	$('#filter-range').slider({
		// formatter: function(value) {
		// 	var valueStr = String(value);
		// 	return valueStr.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
		// 	}	
	});
};


//===========INFO-CLICK
function infoClick(){
	$('.infoBtn').on('click', function(){
		var $this = $(this);
		$this.parents('.item-info').mouseleave(function(){
			$this.removeClass('active');
			$(this).find('.item-info__info').slideUp(200);
		});
		if(!$this.hasClass('active')){
			$this.parents('.item-info').find('.item-info__info').slideDown(200).css({'display':'flex'});
			$this.addClass('active');
		} else {
			var infoWIndow = $this.parents('.item-info').find('.item-info__info');
			infoWIndow.slideUp(200);
			$this.removeClass('active');
		}
	});	
};

// HORIZONTAL_VIEW
function horizontalView(){
	$('#horizontal').on('click',function(){
		$(this).addClass('active').siblings().removeClass('active');
		$('.catalog-items').removeClass('tiles').addClass('horizontal');
	});
	$('#tiles').on('click',function(){
		$(this).addClass('active').siblings().removeClass('active');
		$('.catalog-items').addClass('tiles').removeClass('horizontal');
	});
};

// FIXED_HEADER_MENU
function fixMenu(event){
	var header = $('.header');
	var hedaerHeight = header.outerHeight();
	var menuHight = $('.main-menu').outerHeight();
	var $scrollTop = $(document).scrollTop();
	if(hedaerHeight <= $scrollTop){
		$('.main-menu').addClass('fixed');
		header.css({'margin-bottom': menuHight + 'px'});
	} else {
		$('.main-menu').removeClass('fixed');
		header.removeAttr('style');
	}
};

// TABS_FUNCTION
function tabsSlider(){
	$('.tabsSliderFour').slick({
	  slidesToShow: 4,
	  slidesToScroll: 1,
	  dots: false,
	  draggable:false,
	  responsive:[
	  	{
	  		breakpoint: 1200,
	  		settings:{
	  			slidesToShow: 3
	  		}
	  	},{
	  		breakpoint: 992,
	  		settings:{
	  			slidesToShow: 2
	  		}
	  	}
	  ]
	});
	$('.tabsSliderThree').slick({
	  slidesToShow: 3,
	  slidesToScroll: 1,
	  dots: false,
	  draggable:false,
	  responsive:[
	  	{
	  		breakpoint: 1200,
	  		settings:{
	  			slidesToShow: 2
	  		}
	  	},{
	  		breakpoint: 992,
	  		settings:{
	  			slidesToShow: 2
	  		}
	  	}
	  ]
	});
};

// ITEMS_SLIDER_INIT
function itemsSliderSixInit(name) {

	$(name).slick({
	  dots: false,
	  infinite: true,
	  slidesToShow: 6,
	  slidesToScroll: 1,
	  responsive: [
	  	{
	  		breakpoint: 1400,
	  		settings:{
	  			slidesToShow: 5
	  		}
	  	},{
	  		breakpoint: 1200,
	  		settings:{
	  			slidesToShow: 4
	  		}
	  	},{
	  		breakpoint: 992,
	  		settings:{
	  			slidesToShow: 3
	  		}
	  	}
	  ]
	});

}
function itemsSliderFourInit(name) {

	$(name).slick({
	  dots: false,
	  infinite: true,
	  slidesToShow: 4,
	  slidesToScroll: 1,
	  responsive: [
		{
	  		breakpoint: 1200,
	  		settings:{
	  			slidesToShow: 3
	  		}
	  	},{
	  		breakpoint: 992,
	  		settings:{
	  			slidesToShow: 2
	  		}
	  	}
	  ]
	});

};

// COUNT_SPINNER
 function spinnerCountInit(){
 	$('.countSpinner').spinner({
 		min:1,
 		max:999
 	});
 };

// SELECT_MENU_EDIT
function selectMenuInit(){
	$( ".selectInit" ).selectmenu();
};

// PRODUCT_PHOTO_PREVIEW_FUNCTION
function cartProductImagesInit(){
	$("#img_01").bind("click", function(e) {  
	  var ez =   $('#img_01').data('elevateZoom');	
		$.fancybox(ez.getGalleryList());
	  return false;
	});
	$("#img_01").elevateZoom({
		gallery : 'item-gallery',
		galleryActiveClass : 'active',
		imageCrossfade : true,
		responsive:true
	});
};

function cartInfoTabsInit() {
	$( ".tabs" ).tabs();
};

//   TOOLTIPS_INIT
function tooltipsInit() {
  $('.tooltipBtn').tooltip()
};

//    INIT_REVIEW_STRARS_MARK
function initReviewStars(){
	$('.review-mark').barrating({
    theme:'fontawesome-stars'
  });
};

//    INIT_VERTICAL_SLIDER
function initVerticalSlider(){
	$('.galleryVertical').slick({
	  dots: false,
	  vertical: true,
	  swipe: false,
	  loop: false,
	  infinite: false,
	  slidesToShow: 4,
	  slidesToScroll: 1
	 });
};

//    OPEN_COMPLAIN_POPUP
function openComplainPopup(){
	var popup = $('.complainForm');
	$('.modal').on('click', function(e){
		var $this = $(this);
		var btn = $(e.target);
		if(btn.hasClass('openComplainForm') && !btn.hasClass('active')){
			popup.show();
			btn.addClass('active');
		} else if(!btn.parents().hasClass('complainForm')){
			popup.hide();
			$('.openComplainForm').removeClass('active');			
		}
	});
};

//    SHOW_REVIEW_FORM
function showReviewForm(argument) {
	var messageForm =
				'<form action="#">'+
					'<textarea placeholder="Написать вопрос"></textarea>'+
					'<div class="message__buttons">'+
						'<button class="btn cancel cancelBtn">Отмена</button>'+
						'<button class="btn submit">Отправить</button>'+
					'</div>'+
				'</form>';
	$('.reviews-wrapper').on('click', function(e){
		var target = $(e.target);
		if(target.hasClass('answerFormOpen')){
			target.parent().addClass('active');
			target.parent().next().html(messageForm);
		}
		if(target.hasClass('cancelBtn')){
			e.preventDefault();
			var parent = target.parents('.message');
			parent.html('');
			parent.prev().removeClass('active');
		}
	});
}