function compareSLiderInit(){
	$('.compareSlider').slick({
	  dots: false,
	  infinite: false,
	  slidesToShow: 4,
	  asNavFor: '.compareSliderInfo',
	  draggable:false,
	  slidesToScroll: 1,
	  responsive: [
	  	{
	  		breakpoint: 1200,
	  		settings:{
	  			slidesToShow: 3
	  		}
	  	},{
	  		breakpoint: 992,
	  		settings:{
	  			slidesToShow: 2
	  		}
	  	}
	  ]
	});

	$('.compareSliderInfo').slick({
	  dots: false,
	  arrows:false,
	  infinite: false,
	  slidesToShow: 4,
	  asNavFor: '.compareSlider',
	  draggable:false,
	  slidesToScroll: 1,
	  responsive: [
	  	{
	  		breakpoint: 1200,
	  		settings:{
	  			slidesToShow: 3
	  		}
	  	},{
	  		breakpoint: 992,
	  		settings:{
	  			slidesToShow: 2
	  		}
	  	}
	  ]
	});
};