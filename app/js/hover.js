// HOVER_SIDEBAR_CATALOG
function hoverBlackout(name,parent){
	$(name).mouseenter(function(){
		mouseOn(name,parent);		
	});
	$(name).mouseleave(function(){
		mouseOver(name,parent);
	});
};

function mouseOn(name,parent){
	$(name).css({'z-index':1001});
	$(parent).css({'z-index':1001});
	$('.hover-background').addClass('active');
};
function mouseOver(name,parent){
	$('.hover-background').removeClass('active');
	$(name).removeAttr('style');
	$(parent).removeAttr('style');
};

//  HOVER_MENU_ITEMS

function hoverMenuItems(){
	var unhoverTimeoutIndex;
	$('.filter-drop__item').hover(
		function () {
			// debugger;
			if($(this).hasClass('open')){
				clearTimeout(unhoverTimeoutIndex);
				return;
			}
			$(this).addClass('open');
		},
		function() {
			var $this = $(this);

			unhoverTimeoutIndex = setTimeout(function(){
				$this.removeClass('open')
			}, 250);
		}
	);
};

function closeMenu(){

}