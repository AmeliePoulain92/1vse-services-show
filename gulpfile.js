var gulp 				   = require('gulp'),
		sass 				   = require('gulp-sass'),
    cssComb        = require('gulp-csscomb'),
		browserSync    = require('browser-sync'),
		reload         = browserSync.reload,
    useref         = require('gulp-useref'),
		gulpif         = require('gulp-if'),
		uglify         = require('gulp-uglify'),
		minifyCss      = require('gulp-minify-css'),
		rename         = require('gulp-rename'),
    rigger         = require('gulp-rigger'),
    prettify       = require('gulp-html-prettify'),
    autoprefixer   = require('gulp-autoprefixer'),
    sourcemaps     = require('gulp-sourcemaps'),
    imageMin       = require('gulp-imagemin'),
    wiredep        = require('wiredep').stream,
    jsValidate     = require('gulp-jsvalidate'),
    notify         = require('gulp-notify'),
    plumber        = require('gulp-plumber'),
    spritesmith    = require('gulp.spritesmith'),
    svgSprite      = require("gulp-svg-sprites");

//====================================HTML_TASK
  gulp.task('rigger', function () {
    gulp.src('app/pages/*.html')
      .pipe(rigger())
      .pipe(gulp.dest('app/'))
      .pipe(reload({
        stream:true
      }));
  });
  
  gulp.task('bower', function () {
    gulp.src('app/modules/*.html')
      .pipe(wiredep({
        'ignorePath': '../',
        packages:
          {
            js: [ 'libs/' ],
            css: [ 'libs/' ]
          }
      }))
      .pipe(gulp.dest('app/modules/'))
      .pipe(reload({stream:true}));
  });
//====================================HTML_TASK_END

//====================================COMPILE_SASS_FILES
gulp.task('sass', function(){
  return gulp.src([
                    'app/scss/pages/*.scss',
                    'app/scss/assets/*.scss',
                    'app/scss/modules/*.scss',
                    'app/scss/partial/*.scss',
                    'app/scss/media/*.scss'
                  ])
     .pipe(sourcemaps.init())
     .pipe(sass())
     .on('error', notify.onError(function(err){
        return {
          title: 'Styles',
          message: err.message
        };
       }))
     .pipe(sourcemaps.write())
     // .pipe(autoprefixer({
     //    browsers: ['last 150 versions','ie 9'],
     //    cascade: false
     //  }))
     // .pipe(cssComb())
     .pipe(gulp.dest('app/css'))
     .pipe(reload({stream:true}));
});
//====================================COMPILE_SASS_FILES_END

//====================================COMPILE_JAVASCRIPT_FILES
  gulp.task('scripts', function () {
    gulp.src('app/js/*.js')
      .pipe(plumber({
        errorHendler: notify.onError(function(err){
          return {
            title: 'JavaScript',
            message: err.message
          };
         })
      }))
      .pipe(jsValidate())
      .on('error', notify.onError())
      .pipe(reload({stream:true}));
  });
//====================================COMPILE_JAVASCRIPT_FILES_END

//====================================BROWSER_SYNC
gulp.task('browser-sync', function(dir){
  browserSync({
    port: 9990,
    server: {
      baseDir: './app/'
      // startPath: '/'
    },
    notify: false
  });
});
//====================================BROWSER_SYNC_END

//====================================WATCH
  gulp.task('default', ['rigger','sass'], function(){
    gulp.watch(['app/scss/*/*.scss' ],['sass']);
    gulp.watch("app/*.html").on('change', browserSync.reload);
    gulp.watch(['app/js/**/*.js'],['scripts']);
    gulp.watch(['bower.json'],['bower']);
    gulp.watch(['app/pages/*.html','app/modules/**/*.html','app/modules/*.html'],['rigger']);
    gulp.start('browser-sync');
  });
//====================================WATCH_END

//=============CREATE_SPRITE
gulp.task('sprite', function() {
  var spriteData = 
    gulp.src('./app/images/sprite/*.*') // путь, откуда берем картинки для спрайта
      .pipe(spritesmith({
        imgName: 'sprite.png',
        cssName: 'sprite.css',
        imgPath: '../images/sprite.png'
      }));
  spriteData.img.pipe(gulp.dest('./app/images/')); // путь, куда сохраняем картинку
  spriteData.css.pipe(gulp.dest('./app/css/')); // путь, куда сохраняем стили
});
//=============END:CREATE_SPRITE

// =========== sprite-svg:app ================
gulp.task('sprite-svg:app', function(){
  gulp.src('./app/images/sprites/*.svg')
  .pipe(svgSprite({
    preview: false,
    svg: {
          sprite: 'sprite.svg',
    },
    svgPath: '../images/sprite.svg',
    cssFile: '../scss/assets/_sprite-svg.scss',
    tepmlates: {
      scss: true
    },
  }))
  .pipe(gulp.dest('./app/images'));
});
// =========== END:sprite-svg:app ================

//====================================BUILD_END
  gulp.task('html:build',['useref'], function () {
      return gulp.src('dist/*.html')
          .pipe(prettify({indent_size: 2}))
          .pipe(gulp.dest('dist'));
  });
  gulp.task('useref', function () {
      return gulp.src('app/*.html')
          .pipe(useref())
          .pipe(gulp.dest('dist'));
  });
  gulp.task('scripts:build', function () {
      return gulp.src(['dist/js/*.js','!dist/js/*.min.js'])
      		.pipe(uglify())
      		.pipe(rename(function(path){
          	path.dirname += "/js";
  			    path.extname = ".min.js"
          }))
          .pipe(gulp.dest('dist'));
  });
  gulp.task('styles:build', function () {
      return gulp.src(['dist/css/*.css','!dist/css/*.min.css'])
          .pipe(minifyCss())
          .pipe(rename(function(path){
          	path.dirname += "/css";
  			    path.extname = ".min.css"
          }))
          .pipe(gulp.dest('dist'));
  });
  gulp.task('imageMin:build', function(){
    gulp.src(['app/images/*','app/images/**/*'])
        .pipe(imageMin())
        .pipe(gulp.dest('dist/images'))
  });
  gulp.task('fonts:build', function(){
    gulp.src('app/fonts/**/*')
        .pipe(gulp.dest('dist/fonts/'))
    gulp.src('app/libs/font-awesome/fonts/*.+(eot|woff|woff2|ttf|otf|svg)')
        .pipe(gulp.dest('dist/fonts'))
    gulp.src('app/*.+(png|ico|jpg)')
        .pipe(gulp.dest('dist/'))
  });
  gulp.task('build',['sass','rigger'], function(){ //==============BILD_ALL_PROJECT
    gulp.start('html:build');
    gulp.start('fonts:build');
    gulp.start('scripts:build');
    gulp.start('styles:build');
    gulp.start('imageMin:build');
  });
//====================================BUILD_END